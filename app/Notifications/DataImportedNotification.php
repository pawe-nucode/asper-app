<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DataImportedNotification extends Notification
{
    use Queueable;

    protected $filename;
    protected $addedDataStats;

    public function __construct(string $filename, array $addedDataStats)
    {
        $this->filename = $filename;
        $this->addedDataStats = $addedDataStats;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $statsLine = '<table><tbody>
<tr><th>Cities added: </th><td>'.$this->addedDataStats['citiesAdded'].'</td></tr>
<tr><th>Streets added: </th><td>'.$this->addedDataStats['streetsAdded'].'</td></tr>
<tr><th>Years added: </th><td>'.$this->addedDataStats['yearsAdded'].'</td></tr>
<tr><th>Object added: </th><td>'.$this->addedDataStats['cityObjectsAdded'].'</td></tr>
</tbody></table>';

        return (new MailMessage)
                    ->line('Imported new data from excel file: '.$this->filename)
                    ->line($statsLine)
                    ->line('Thank you for using our application!');
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
