<?php

namespace App\Console\Commands;

use App\Notifications\DataImportedNotification;
use App\Services\ExcelService;
use App\User;
use Illuminate\Console\Command;

class importXlsx extends Command
{

    protected $signature = 'import:xlsx {filename} {--send-notification=true}';
    protected $description = "Imports XLSX file to database. Usage: php artisan import:xlsx filename.xlsx [--send-notification=true/false (default true)].";

    protected $excelService;

    public function __construct(ExcelService $excelService, User $user)
    {
        $this->excelService = $excelService;
        $this->user = $user;

        parent::__construct();
    }

    public function handle()
    {
        $excelFilename = $this->argument('filename');

        try {
            $this->excelService->loadExcelFile($excelFilename);
            $addedDataStats = $this->excelService->updateDatabase();
        } catch (\Throwable $e) {
            echo "Something went wrong: " . $e->getMessage() . "\n";
            return;
        }

        $this->showMessage($addedDataStats);
        $this->showErrors($addedDataStats);

        if (!$this->option('send-notification')) {
            return;
        }

        try {
            $this->sendNotificationToAdministrator($excelFilename, $addedDataStats);
        } catch (\Throwable $e) {
            echo "Something went wrong while sending e-mail notification: ".$e->getMessage();
            return;
        }

        echo "Notification e-mail sent to administrator!\n";

        return;
    }

    private function showMessage(array $addedDataStats): void
    {
        $totalUpdates = $addedDataStats['citiesAdded'] + $addedDataStats['streetsAdded'] + $addedDataStats['yearsAdded'] + $addedDataStats['cityObjectsAdded'];

        echo "----------------------------\n";

        if (!$totalUpdates) {
            echo "There are no new data, database not updated.\n";
            return;
        }

        echo "Database updated: \n";
        echo "----------------------------\n";
        echo 'New cities added: ' . $addedDataStats['citiesAdded']."\n";
        echo 'New streets added: ' . $addedDataStats['streetsAdded']."\n";
        echo 'New years added: ' . $addedDataStats['yearsAdded']."\n";
        echo 'New cities objects added: ' . $addedDataStats['cityObjectsAdded']."\n";


    }

    private function showErrors(array $addedDataStats): void
    {
        if (count($addedDataStats['errors'])) {
            echo "----------------------------\n";
            echo "There were errors: \n";
            echo "----------------------------\n";
            echo join("\n", $addedDataStats['errors']);

        }
    }

    private function sendNotificationToAdministrator(string $excelFilename, array $addedDataStats): void
    {
        $user = $this->user->where('name', 'administrator')->first();
        $user->notify(new DataImportedNotification($excelFilename, $addedDataStats));
    }
}
