<?php

namespace App\Http\Controllers;

use App\Models\City;

class CitiesObjectsController
{
    protected $city;

    public function __construct(City $city)
    {
        $this->city = $city;
    }

    public function getTable()
    {
        return response()->JSON($this->city->with('streets', 'streets.years', 'streets.years.cityObjects')->get());
    }
}