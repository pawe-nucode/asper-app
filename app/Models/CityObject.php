<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityObject extends Model
{
    protected $table = 'objects';
    protected $fillable = ['name', 'year_id'];

    public function street() {
        return $this->belongsTo(Street::class);
    }

}