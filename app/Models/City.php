<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = ['name'];

    public function streets()
    {
        return $this->hasMany(Street::class);
    }

    public function years()
    {
        return $this->hasManyThrough(Year::class, Street::class, 'city_id', 'street_id', 'id', 'id');
    }

    public function cityObjects()
    {
        return $this->hasManyThrough(Year::class, Street::class, 'city_id', 'street_id', 'id', 'id')->join('objects',
            'years.id', '=', 'objects.year_id')->select('objects.*');
    }

}
