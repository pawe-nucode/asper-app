<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $table = 'years';
    protected $fillable = ['year', 'street_id'];

    public function street()
    {
        return $this->belongsTo(Street::class);
    }

    public function cityObjects()
    {
        return $this->hasMany(CityObject::class);
    }
}
