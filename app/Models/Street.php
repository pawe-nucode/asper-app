<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    protected $table = 'streets';
    protected $fillable = ['name', 'city_id'];

    public function city() {
        return $this->belongsTo(City::class);
    }

    public function years() {
        return $this->hasMany(Year::class);
    }

    public function objects() {
        return $this->hasManyThrough(CityObject::class, Year::class, 'street_id', 'street_id', 'id', 'id');
    }
}