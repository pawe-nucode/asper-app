<?php

namespace App\Services;

use Maatwebsite\Excel\Excel;
use App\Models\City;
use App\Models\Street;
use App\Models\Year;
use App\Models\CityObject;
use Illuminate\Support\Collection;

class ExcelService
{

    protected $excel;
    protected $city;
    protected $street;
    protected $year;
    protected $cityObject;

    protected $filename;
    protected $excelDataCollection;

    protected $citiesAdded = 0;
    protected $streetsAdded = 0;
    protected $yearsAdded = 0;
    protected $cityObjectsAdded = 0;
    protected $errors = [];


    public function __construct(Excel $excel, City $city, Street $street, Year $year, CityObject $cityObject)
    {
        $this->excel = $excel;
        $this->city = $city;
        $this->street = $street;
        $this->year = $year;
        $this->cityObject = $cityObject;
    }

    public function loadExcelFile(string $filename)
    {
        $this->filename = $filename;
        $this->excelDataCollection = $this->excel->toCollection(collect(), $this->filename, 'excel_files');
    }

    public function updateDatabase(): array
    {
        $this->excelDataCollection->each(function ($rows) {
            $rows->shift(); // remove first row (header)
            $rows->each(function ($row, $index) {
                try {
                    $this->insertRow($row);
                } catch (\Throwable $e) {
                    $index+=2; // rows numeration in excel starts from 1 and we dropped header row from collection so we must add 2 to index number
                    $this->errors[] = 'Error while trying to insert data from row ' . $index. ': ' . $e->getMessage();
                }
            });
        });

        return [
            'citiesAdded'      => $this->citiesAdded,
            'streetsAdded'     => $this->streetsAdded,
            'yearsAdded'       => $this->yearsAdded,
            'cityObjectsAdded' => $this->cityObjectsAdded,
            'errors'           => $this->errors
        ];
    }

    private function insertRow(Collection $row)
    {
        $yearValue = $row->shift();
        $cityName = $row->shift();
        $streetName = $row->shift();
        $cityObjectName = $row->shift();

        $city = $this->addCity($cityName);
        $street = $this->addStreet($streetName, $city);
        $year = $this->addYear($yearValue, $street);
        $this->addCityObject($cityObjectName, $year);
    }

    private function addCity(string $name): City
    {
        $city = $this->city->firstOrCreate(['name' => $name]);

        if ($city->wasRecentlyCreated) {
            $this->citiesAdded++;
        }

        return $city;
    }

    private function addStreet(string $name, City $city): Street
    {
        $street = $this->street->firstOrCreate(['name' => $name, 'city_id' => $city->id]);

        if ($street->wasRecentlyCreated) {
            $this->streetsAdded++;
        }

        return $street;
    }

    private function addYear(string $yearValue, Street $street): Year
    {
        $year = $this->year->firstOrCreate(['year' => $yearValue, 'street_id' => $street->id]);

        if ($year->wasRecentlyCreated) {
            $this->yearsAdded++;
        }

        return $year;
    }

    private function addCityObject(string $name, Year $year): CityObject
    {
        $cityObject = $this->cityObject->firstOrCreate(['name' => $name, 'year_id' => $year->id]);

        if ($cityObject->wasRecentlyCreated) {
            $this->cityObjectsAdded++;
        }

        return $cityObject;
    }

}