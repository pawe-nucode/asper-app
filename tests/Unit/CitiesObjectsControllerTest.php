<?php

namespace Tests\Unit;

use App\Models\City;
use App\Models\CityObject;
use App\Models\Street;
use App\Models\Year;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CitiesObjectsControllerTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    private $jsonStructure;

    public function testGetTable()
    {
        $city = factory(City::class)->create();
        $street = factory(Street::class)->create(['city_id' => $city->id]);
        $year = factory(Year::class)->create(['street_id' => $street->id]);

        factory(CityObject::class)->create(['year_id' => $year->id]);

        $city->load('streets', 'streets.years', 'streets.years.cityObjects');
        $json = $city->toArray();

        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->json('GET', '/cities-objects/');
        $response->assertStatus(200)->assertJsonFragment($json);
    }
}
