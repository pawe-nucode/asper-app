<?php

namespace Tests\Unit;

use App\Models\City;
use App\Models\CityObject;
use App\Models\Street;
use App\Models\Year;
use App\Services\ExcelService;
use Factory;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\QueuedWriter;
use Maatwebsite\Excel\Reader;
use Maatwebsite\Excel\Writer;
use Tests\TestCase;

class ExcelServiceTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    private $jsonStructure;

    public function testUpdateDatabase()
    {
        $excelService = new ExcelService(new Excel(new Writer ,new QueuedWriter(new Writer()), new Reader(new FilesystemManager($this->app)), new FilesystemManager($this->app)), new City(), new Street(), new Year(), new CityObject());

        $excelService->loadExcelFile('parser_test.xlsx');
        $result = $excelService->updateDatabase();

        $this->assertIsArray($result);

        $this->assertArrayHasKey('errors', $result);
        $this->assertArrayHasKey('citiesAdded', $result);
        $this->assertArrayHasKey('streetsAdded', $result);
        $this->assertArrayHasKey('yearsAdded', $result);
        $this->assertArrayHasKey('cityObjectsAdded', $result);

        $this->assertIsArray($result['errors']);
        $this->assertEmpty($result['errors']);

        $this->assertEquals($result['citiesAdded'], 1);
        $this->assertEquals($result['streetsAdded'], 1);
        $this->assertEquals($result['yearsAdded'], 1);
        $this->assertEquals($result['cityObjectsAdded'], 1);

        $this->assertDatabaseHas('cities', ['name' =>'testCity1']);
        $this->assertDatabaseHas('streets', ['name' =>'testStreet1']);
        $this->assertDatabaseHas('years', ['year' => 2100]);
        $this->assertDatabaseHas('objects', ['name' =>'cityObject1']);
    }
}
