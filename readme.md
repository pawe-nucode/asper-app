# Simple training Laravel app #

First run:

- Setup database in .env.
- Set DEFAULT_ADMIN_EMAIL in .env - it will be used as default administrator account (as login, and also as e-email for notifications). If empty - hello@example.com will be used.
- Run migrations and database seeder: ```php artisan migrate --seed``` - this will add user "administrator" with e-mail defined in .en

## Importing xlsx files ##

CLI command for import xlsx files:

```php artisan import:xlsx``` filename.xlsx

xlsx files are stored in ```storage/excel_files``` directory (see config/filesystems.php)

Please do not delete parser_test.xlsx file - without this file parser test will fail.

## Basic frontend ##

If you are running application using asper-docker repository go to

http://asper.local/login

Default login data: e-mail as above (see First run section), password: secret

After login you should be redirected to http://asper.local/home.
