<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAdministrator = new User([
                'name'                      => 'administrator',
                'email'                     => env('DEFAULT_ADMIN_EMAIL', 'hello@example.com'),
                'password'                  => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            ]
        );

        $userAdministrator->save();
    }

}
