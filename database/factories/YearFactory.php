<?php

use Faker\Generator as Faker;
use App\Models\Year;
use App\Models\Street;

$factory->define(Year::class, function (Faker $faker) {
    return [
        'year' => $faker->year,
        'street_id' => function () {
            return factory(Street::class)->create()->id;
        }
    ];
});
