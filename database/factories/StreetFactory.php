<?php

use Faker\Generator as Faker;
use App\Models\Street;
use App\Models\City;

$factory->define(Street::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'city_id' => function () {
            return factory(City::class)->create()->id;
        }
    ];
});
