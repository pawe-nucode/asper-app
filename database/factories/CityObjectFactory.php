<?php

use Faker\Generator as Faker;
use App\Models\CityObject;
use App\Models\Year;

$factory->define(CityObject::class, function (Faker $faker) {
    return [
        'name' => $faker->text(100),
        'year_id' => function () {
            return factory(Year::class)->create()->id;
        }
    ];
});
